<?

class NullException extends Exception
{
  
  public $data;

  function __construct($message, $data = null)
  {
    parent::__construct($message);
    $this->data = $data;
  }
  
}

?>
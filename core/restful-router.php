<?

header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");  // disable IE caching
header("Last-Modified: " . gmdate( "D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-cache, must-revalidate");
header("Pragma: no-cache");

require_once 'vendor/php-activerecord/ActiveRecord.php';
require_once "./../config/config.php";
require_once "restful-controller.php";
require_once "null-exception.php";

class RESTfulRouter
{
	private $controllers;
	function __construct($controllers)
	{
		$this->controllers = $controllers;
		ActiveRecord\Config::initialize(function($cfg)
		{
		    $user = Config::$db_username;
		    $pass = Config::$db_password;
		    $host = Config::$db_host;
		    $dbname = function()
		    {
		        switch(Config::$environment)
		        {
		            case "development": return Config::$db_development_name; 
		            case "test": return Config::$db_test_name;
		            case "production": return Config::$db_production_name;
		        }
		    };
		    $dbname = $dbname();
		    $cfg->set_model_directory('models');
		    ActiveRecord\Serialization::$DATETIME_FORMAT = "Y-m-d";
		    $cfg->set_connections(array(
		    'development' => "mysql://$user:$db_password@$host/$dbname"));
		});
	}

	function run()
	{
		$_SERVER['REQUEST_URI'] = str_replace("/api", "", $_SERVER['REQUEST_URI']);
		$uri = $_SERVER['REQUEST_URI'];
		$a = explode("?", $uri);
		$uri = $a[0];
		$tok = substr($uri, 1, strlen($uri));
		$toks = explode("/", $tok);
		$t = toks[0];
		$params = [];
		foreach($toks as $_t)
		{
			if(!is_numeric($_t) and strlen($_t) > 0)
			{
				$t = $_t;
			}
			else 
				if(is_numeric($_t))
					$params[substr($t, 0, strlen($t) - 1) . '_id'] = $_t;

		}
		if(in_array($t, $this->controllers))
		{
			$name = ucwords($t)."Controller";
      include("restful/".$t.".php");
			$c = new $name($params);
      
      $answer = new stdClass();
      try 
      {
        $result = $c->run();
			 	$answer->status = "ok";
			 	$answer->response = $result;
			 	echo json_encode($answer);
			} 
			catch (Exception $e) 
			{
				$answer->status = "error";
				$answer->error = ['message' => (get_class($e) != "NullException"?'Backend exception: ':'') .$e->getMessage()];
				if(isset($e->data))
					$answer->error['data'] = $e->data;
				echo json_encode($answer);
			} 

		}
		else
			echo "Unknown controller!";
	}

}

$registered_controllers = [
  "items",
  "models",
  ];
$router = new RESTfulRouter($registered_controllers);
$router->run();




?>
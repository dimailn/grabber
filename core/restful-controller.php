<?

class RESTfulController
{
	protected $id;
	private $name;
	function __construct($params)
	{
		$this->params = $params;
		$_SERVER['REQUEST_URI'] = str_replace("/api", "", $_SERVER['REQUEST_URI']);
		$toks = explode("/", $_SERVER['REQUEST_URI']);
		if(count($toks) == 2 or count($toks) > 3)
			$this->id = null;
		else
			$this->id = $toks[2];
		$this->dbh = ActiveRecord\Connection::instance();
	}

	function run()
	{
		$this->name = $this->get_ar_name();
		switch($_SERVER['REQUEST_METHOD'])
		{
			case "GET": 
				if($this->id !== null)
					return $this->show($this->id); 
				else
					return $this->index();
			break;
			case "POST":
				$_method = $_POST['_method'];
				if(isset($_method))
				{
					switch($_method)
					{
						case 'PUT':
							unset($_POST['_method']);
							return $this->update($this->id); 
						break;
						case 'DELETE': return $this->destroy($this->id); break;
					}
				}
				else
					return $this->create();
			break;
		}
	}

	private function get_ar_name()
	{
		$name = str_replace("Controller", "", get_class($this));
		$name = substr($name, 0, strlen($name) - 1);
		return $name;
	}




	function index()
	{
		
		$name = $this->name;
		$c_name = get_class($this);
		if(isset($c_name::$belongs_to))
		{
			foreach($this->params as $key => $param)
			{
				$k = explode("_", $key);
				$k = $k[0];
				if(in_array($k, $c_name::$belongs_to))
				{
					return $this->all_array($name::all(["conditions" => ["$key = ?", $param]]));
				}
			}
		}
		else 
			return $this->all_array($name::all());
	}



	function create()
	{

		$data = json_decode($_POST['data']);

		$name = $this->name;
		$this->rebuild_post();


		if(gettype($data) == "array")
		{
		  $resources = $data;
		  foreach($resources as $resource)
		  {
		      if(!$name::create((array)$resource)->id)
		          throw new Exception("Не удалось добавить один из экземпляров $name.");
		  }
		  return true;
		}
		else
			return ["id" => (int)$name::create($_POST)->id];

	}

	function update($id)
	{
		$data = json_decode($_POST['data']);

		$name = $this->name;
		$this->rebuild_post();

		if(gettype($data) == "array")
		{
		  $resources = $data;
		  foreach($resources as $resource)
		  {
		  	  $id = $resource->id;
		  	  unset($resource->id);
		      if(!$name::find_by_id($id)->update_attributes((array)$resource))
		          throw new Exception("Не удалось обновить один из экземпляров $name.");
		  }
		  return true;
		}
		else
			return $name::find_by_id($id)->update_attributes($_POST);
	}

	function show($id)
	{
		$name = $this->name;
		return $name::find_by_id($id)->to_array();
	}

	function destroy($id)
	{
		$name = $this->name;
		return $name::find_by_id($id)->delete();
	}

	//Utils
	function rebuild_post()
	{
		$_POST = (array)json_decode($_POST['data']);
	}

	function all_array($array)
	{
			$arr = [];
	    foreach($array as $value)
	    {
	        $arr[] = $value->to_array();
	    }
	    return $arr;
	}

}


?>
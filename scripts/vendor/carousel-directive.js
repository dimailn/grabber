
angular.module('App').directive('carousel', carousel);

function carousel() {
    return {
        restrict: 'A',
        replace: true,
        transclude: false,
        scope: {
            images: "="
        },
        template: '<div class="jcarousel-wrapper"><div class="jcarousel"><ul><li ng-repeat="img in images">{{img}}</li></ul></div><a href="javascript:void(0)" class="jcarousel-control-prev">&lsaquo;</a><a href="javascript:void(0)" class="jcarousel-control-next">&rsaquo;</a></div>',
        link: function link(scope, element, attrs) {
            var container = $(element);
            var carousel = container.children('.jcarousel');

            carousel.jcarousel({
                wrap: 'circular'
            });

            scope.$watch(attrs.images, function (value) {
                carousel.jcarousel('reload');
            });

            container.find('.jcarousel-control-prev')
                .jcarouselControl({
                target: '-=1'
            });

            container.find('.jcarousel-control-next')
                .jcarouselControl({
                target: '+=1'
            });
        }
    };
}
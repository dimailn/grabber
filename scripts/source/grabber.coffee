fs = require "fs"
path = require "path"
walk = require "walk"
cheerio = require "cheerio"


cat_name = "www.fa-za.ru"
# cat_name = "jazz-way.com"
walker = walk.walk("./../../pages/#{cat_name}/")

get_extension = (str) ->
  toks = str.split(".")
  toks[toks.length - 1]

i = 0
products = []
walker.on "file", (root, fileStat, next) ->
  if get_extension(fileStat.name) == "htm"
    fs.readFile path.resolve(root,  fileStat.name), "utf8", (err, file) ->
      # console.log path.resolve(root,  fileStat.name)
      $ = cheerio.load file






      hierarchy = $("#breadcrumb .content").text().trim()
      if hierarchy.length
        toks = hierarchy.split " / "
        parent_title = toks[toks.length - 1]
      parent = name: parent_title, children: []
      # Грабим категории с изображениями
      $(".product").each (index, product) -> 
        title = $(product).find("span.title")
        image = $(product).find("span.image img").attr("src")
        parent.children.push name: $(title).text().trim(), image: image

      product = $($("#product")[0])
      if product?
        #Грабим модели
        models = []
        product.find("div.description .table tbody tr").each (index, model) ->
          _model = []
          $(model).find("td").each (i, td) -> _model.push $(td).text().trim()
          models.push _model
        # Грабим детали
        details = []
        product.find("div.description div.list ul li").each (index, detail) ->
          details.push $(detail).text().trim()

        # Грабим картинки
        images = []
        product.find("div.jcarousel ul li img").each (index, image) ->
          images.push $(image).attr("src")

        parent.models = models
        parent.details = details
        parent.images = images



      products.push parent
      i++






      next()
  else
    next()

walker.on "end", -> 
  result = {}
  result.categories = products.filter (product) -> product.children.length
  result.items = products.filter (product) -> not product.children.length
  console.log JSON.stringify result
# fs.readdir "./../../../", (err, files) ->
#   console.log files
class Model extends ActiveData
  @controller_name "Model"
  @set_keys ["item_id", "name","cost"]
  constructor: ->
    super

class Item extends ActiveData
  @controller_name "Item"
  @set_keys ["name"]
  @instance_contains Model
  constructor: ->
    super

ActiveData.set_date_format "yyyy-mm-dd"
class MainCtrl

  constructor: (@$scope, @$timeout) ->
    @models = []
    # do @parse_images
    do window.init_toggle
    # do @init_ids
    $tree = $("#tree")

    $tree.tree({
        data: window.catalog
        autoOpen: false
    });

    $('#tree').bind(
        'tree.click',
        (event) =>
          @go event.node

        
    )
    @item = window.catalog[0]
    @$timeout -> window.init_shop()

  parse_img_str: (str) ->
    index = str.lastIndexOf "../"
    str.substring index + 3
  parse_images: ->
    window.catalog.treeEach (node) =>
      if node.image?
        node.image = @parse_img_str node.image
        # index = node.image.lastIndexOf "../"
        # node.image = node.image.substring index + 3
      if node.images?
        node.images.forEach (img, index) => node.images[index] = @parse_img_str img

  click: (item) ->
    @go item

  go: (item) ->
    @item = item
    if item.id?
      do @get_models
      @model = new Model item_id: item.id
    @$scope.$apply @
    @$timeout -> window.init_shop()

  add_cost: ->
    @model.save()
      .then (st) => 
        st and toastr.success "Сохранили"
        do @get_models
    @model = new Model item_id: @item.id

  init_ids: ->
    # window.catalog.treeEach (node) -> 
    #   unless node.children? and node.children.length
    #     item = new Item(name: node.name)
    #     item.save().then ->
    #       node.id = item.id
  get_models: ->
    if @item.id?
      Item.find_by_id(@item.id)
        .then((item) => item.get_models())
        .then((@models) => @$scope.$apply @models)
  del_model: (model) ->
    model.destroy()
      .then (st) =>
        if st then toastr.success "Удаление прошло успешно"
        do @get_models







angular.module("App", []).controller("MainCtrl", MainCtrl)
clone = (obj) ->
  if obj == null or typeof obj != 'object'
    return obj
  else
    if obj.getTime != undefined then temp = new Date obj.getTime()
    else 
      temp = new obj.constructor()
      for key of obj
          temp[key] = clone obj[key]
  temp
  

Array.prototype.find = (func) ->
  for e in @
    if func(e) then return e
  undefined
Array.prototype.first = ->
  @[0]
Array.prototype.last = ->
    @[@.length - 1];

Array.prototype.empty = ->
  @splice 0, @length

Array.prototype.treeReduce = (func, value, children_name = "children") ->
  applyFunc = (branch) ->
    for child in branch
      value = func child, value
      if child[children_name].length then applyFunc child[children_name]
  applyFunc @
  value

Array.prototype.treeEach = (func, children_name = "children") ->
  applyFunc = (branch, parent) ->
    for child, index in branch
      func child, parent
      if child[children_name]? and child[children_name].length then applyFunc child[children_name], child, index
  applyFunc @, null
  # @.treeReduce func, undefined,  children_name

Array.prototype.treeMap = (func, children_name = "children") ->
  newTree = clone @
  applyFunc = (branch, parent) ->
    for child in branch
      child = func child, parent
      if child and child[children_name] and child[children_name].length then applyFunc child[children_name], child
  applyFunc newTree, null
  newTree

Array.prototype.childFieldRename = (oldName, newName, array) ->
  newTree = if array? then array else []
  applyFunc = (branch, parent) ->
    for child in branch
      newChild = clone child
      delete newChild[oldName]
      newChild[newName] = []
      parent.push newChild
      if child[oldName].length then applyFunc child[oldName], newChild[newName]
  applyFunc @, newTree
  newTree

Array.prototype.treeFilter = (func) ->
  newTree = clone @
  applyFunc = (branch) ->
    flag = false
    for index in [branch.length - 1 .. 0]
      child = branch[index]
      if func(child) then flag = true
      else
        if child.children.length
          unless applyFunc(child.children)
            branch.splice index, 1
          else
            flag = true
        else
          branch.splice index, 1
    flag

  applyFunc newTree
  newTree

Array.prototype.rise = (condition, what_do_with_parents, children_name = "data") ->
  @treeEach ((node) -> 
    if condition node
      parent = node.parent()
      while parent?
        what_do_with_parents parent
        parent = parent.parent()), children_name


Array.prototype.down = (condition, what_do_with_children, children_name = "data") ->
  @treeEach (node) -> if condition node then node[children_name].treeEach ((_node, children_name) -> what_do_with_children _node), children_name

Array.prototype.treeClone = (children_name) ->
  @treeMap ((node, parent) -> node.parent = (-> parent); node), children_name

Array.prototype.treeFind = (condition, children_name = "children") ->
  match = null

  applyFunc = (branch) ->
    for child in branch
      if match? then break
      if condition(child) then match = child
      if child[children_name]? and child[children_name].length then applyFunc child[children_name]

  applyFunc @, null

  match

Array.prototype.treeMerge = (tree, predicate) ->
  result = clone @
  merge = (first_branch, second_branch) ->
    second_branch? and second_branch.forEach (node) -> 
      collision = first_branch.find (_node) -> predicate node, _node 
      unless collision? then first_branch.push node
      else  merge collision.children, node.children

  merge result, tree
  result




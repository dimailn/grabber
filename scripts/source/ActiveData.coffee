window.classof = (object) ->
  Object.prototype.toString.call(object).slice(8, -1)

class @ActiveData extends Object
  $http = undefined
  base_url: "/api/"
  constructor: (@__hash, @__dirty = true) ->
    @__data = {}
    @id = null
    unless ActiveData::$http then do ActiveData::init_http

    @__pristine_data = {}

    for key in Object.getPrototypeOf(@).__keys
        @__data[key] = 
        if @__hash? 
          if key == "start_date" or key == "end_date" or key == "date"
            new Date @__hash[key]
          else
            @__hash[key] 
        else 
          null
        if @__dirty then @__pristine_data[key] = null
        else @__pristine_data[key] = @__data[key]
        do (key) =>
          Object.defineProperty @, key,
            get: ->
              @__data[key]
            set: (value) ->
              if Object.getPrototypeOf(@).__ro and Object.getPrototypeOf(@).__ro.indexOf(key) != -1 then throw "Вы пытаетесь изменить read-only свойство \"#{key}\" объекта #{Object.getPrototypeOf(@).__name}."
              else
                if (JSON.stringify(@__data[key]) isnt JSON.stringify(value) and key isnt 'id') then @__dirty = true; @__pristine_data[key] = @__data[key]
                @__data[key] = value
            enumerable: true
            configurable:true


  @controller_name: (name) ->
    Object.defineProperty @::, "__name",
      enumerable: false
      writeble: false
      value: name

  @set_keys: (keys) ->
    unless @::__keys?
      Object.defineProperty @::, "__keys",
        enumerable: false
        writebla: false
        value: []

    for key in keys then @::__keys.push key


  @read_only: (array) ->
    unless @::__ro?
      @::__ro = array
    else
      for ro in array then @::__ro.push ro

  @hidden_fields: ->
    unless @::__hidden? then @::__hidden = []
    for a in arguments then @::__hidden.push a

  @all: (nowrap = false) ->
    url = ActiveData::base_url + @.prototype.__name.toLowerCase() + 's'
    unless ActiveData::$http then do ActiveData::init_http
    ActiveData::$http.get(url).then (obj) => 
      switch obj.data.status
        when "ok"
          obj.data.response.reduce ((arr, obj) => 
            unless nowrap
              arr.push new @::constructor(obj, false)
              arr.last().id = obj.id
              arr
            else
              arr
              ), []
        when "error" then throw obj.data.error


  @find_by_id: (id) ->
    url = ActiveData::base_url + @.prototype.__name.toLowerCase() + "s/" + id
    unless ActiveData::$http then do ActiveData::init_http
    @__dirty = false
    ActiveData::$http.get(url).then (obj) =>
      switch obj.data.status
        when "ok"
          o = new @::constructor(obj.data.response, false); o.id = obj.data.response.id; o
        when "error"
          throw obj.data.error


  @start_session: ->
    @::session_data = []
  @end_session: ->
    url = ActiveData::base_url + @prototype.__name.toLowerCase() + 's'
    array = []
    for e in @::session_data then array.push do e._prepare_data
    # Если хоть у одного есть ID - обновление
    obj = {}
    if @::session_data.first().id?
      obj._method = "PUT"
      @::session_data.forEach (e, i) -> array[i].id = e.id
    data = JSON.stringify array
    obj.data = data
      
    ActiveData::$http(
      method: "POST"
      data: $.param obj
      url:url
      headers: 'Content-Type': 'application/x-www-form-urlencoded'
    ).then (obj) => 
      if obj.data.status == "ok" 
        for e in @::session_data then e.__dirty = false; e.__pristine_data = {}
        @::session_data = undefined
        true
      else
        false

  _prepare_data: ->
    date_format = if ActiveData::date_format? then ActiveData::date_format else if @::date_format? then @::date_format else null
    data = $.extend {}, @__data
    for key of @__data
      if date_format and classof(@__data[key]) == "Date" 
        data[key] = @__data[key].format date_format
      if Object.getPrototypeOf(@).__ro and Object.getPrototypeOf(@).__ro.indexOf(key) != -1 then delete data[key]
      if Object.getPrototypeOf(@).__hidden and Object.getPrototypeOf(@).__hidden.indexOf(key) != -1 then delete data[key]
    data

  save: ->
    # Если есть id - тогда update, иначе create
    # url = @__name.toLowerCase()
    body_data = @_prepare_data()
    data = {}
    url = ActiveData::base_url + Object.getPrototypeOf(@).__name.toLowerCase() + 's'

    handler = (obj) -> obj.data.response.id

    if @id?
      data._method = "PUT"
      url += "/" + @id
      handler = (obj) -> obj.data.status == "ok"

    if @__dirty
      if Object.getPrototypeOf(@).session_data then Object.getPrototypeOf(@).session_data.push @
      else
        data.data = JSON.stringify body_data
        ActiveData::$http(
          method: "POST"
          data: $.param data
          url:url
          headers: 'Content-Type': 'application/x-www-form-urlencoded'
        ).then (obj) => 
          switch obj.data.status
            when "ok"
              @__dirty = false
              @__pristine_data = {}
              unless @id then @id = obj.data.response.id
              handler obj
            when "error" then throw obj.data.error

    else
      then: -> console.log "Data is not dirty!"

  destroy: ->
    url = ActiveData::base_url + Object.getPrototypeOf(@).__name.toLowerCase() + "s/" + @id
    ActiveData::$http(
      method: "POST"
      data: $.param _method: "DELETE"
      url:url
      headers: 'Content-Type': 'application/x-www-form-urlencoded'
    ).then (answer) -> 
      if answer.data.status == "ok" then true
      else throw answer.data.error

  @set_date_format: (format) ->
    ActiveData::date_format = format

  set_date_format: (format) ->
    Object.defineProperty @, "_date_format", 
      enumerable: false
      value: format

  init_http: ->
    injector = angular.injector(['ng', 'App'])
    ActiveData::$http = injector.get "$http"

  clear: =>
    if @__dirty
      for key, value of @__pristine_data then if not @__ro or @__ro.indexOf(key) == -1 then @[key] = value
      @__dirty = false
      @__pristine_data = {}

  @class_contains: (c) ->
    name = c::__name.toLowerCase() + 's'
    parent_name = @::__name.toLowerCase() + 's'
    Object.defineProperty @, "get_#{name}", value: =>
      url = @::base_url + "#{parent_name}/#{name}/"
      ActiveData::$http.get(url).then (obj) => obj.data.reduce ((arr, obj) => arr.push new @::constructor(obj, false); arr.last().id = obj.id; arr), []


  @instance_contains: () ->
    parent_name = @::__name.toLowerCase() + 's'
    for c in arguments
      name = c::__name.toLowerCase() + 's'
      do (name, c) =>
        Object.defineProperty @::, "get_#{name}", value: ->
          url = ActiveData::base_url + "#{parent_name}/#{@id}/#{name}/"
          ActiveData::$http.get(url).then (obj) =>
            switch obj.data.status
              when "ok"
                obj.data.response.reduce ((arr, obj) => arr.push new c::constructor(obj, false); arr.last().id = obj.id; arr), []
              when "error"
                throw obj.data.error
     


require "./array_extension.js"
fs = require "fs"

site_name = "fa-za"
# site_name = "jazz-way"
fs.readFile "./../../json/#{site_name}.json", "utf8", (err, file) ->
  result = JSON.parse file
  tree = result.categories
  items = result.items
  tree.treeEach (node, _parent, index) ->
    if node.children 
      parent = tree.treeFind (_node) -> node.name == _node.name and not _node.children
      if parent? 
        parent.children = node.children
        if _parent? then _parent.children.splice index, 1

  tree = tree.filter (node) -> node.name == "Каталог"
  tree.treeEach (node) ->
    unless node.children?
      product = items.find (item) -> item.name == node.name
      if product?
        node.images = product.images
        node.details = product.details
        node.models = product.models
  console.log JSON.stringify tree
      
